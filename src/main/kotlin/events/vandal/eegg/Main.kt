package events.vandal.eegg

import events.vandal.eegg.commands.EggCommand
import events.vandal.eegg.events.EggRightClickEvent
import events.vandal.eegg.utils.Eggs
import hu.trigary.advancementcreator.Advancement
import hu.trigary.advancementcreator.AdvancementFactory
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.Particle
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask

class Main : JavaPlugin() {
    override fun onEnable() {
        plugin = this

        if (!this.dataFolder.exists())
            this.dataFolder.mkdir()

        this.getCommand("egg")?.setExecutor(EggCommand)
        this.getCommand("egg")?.tabCompleter = EggCommand

        this.server.pluginManager.registerEvents(EggRightClickEvent, this)
        //this.server.pluginManager.registerEvents(PlayerAntiDamageEvent, this)

        particleTask = this.server.scheduler.runTaskTimer(this, Runnable {
            Eggs.presents.forEach {
                this.server.onlinePlayers.forEach {itt ->
                    if (!it.value.acquired.contains(itt.uniqueId)) {
                        val loc = Location(it.value.world, it.value.position.blockX + 0.5, it.value.position.blockY + 0.6, it.value.position.blockZ + 0.5)
                        itt.spawnParticle(Particle.VILLAGER_HAPPY, loc, 10)
                    }
                }
            }
        }, 50, 45)

        val factory = AdvancementFactory(this, false, false)

        val root = factory.getRoot("eegg/root", "Easter Eggs", "Time to collect everything!", Material.SHULKER_BOX, "block/dark_oak_trapdoor")
        root.activate(false)

        val wakeUp = factory.getImpossible("eegg/wake_up_call", root, "Wake Up Call", "Collect your first easter egg.", Material.PLAYER_HEAD).setFrame(Advancement.Frame.TASK).setHidden(true)

        val allEggs = factory.getImpossible("eegg/all_eggs", wakeUp, "Coming Out the Closet", "Collect every easter egg in the map.", Material.DARK_OAK_TRAPDOOR).setFrame(Advancement.Frame.CHALLENGE).setHidden(true)
        wakeUp.activate(false)
        allEggs.activate(false)

        Bukkit.reloadData()
    }

    override fun onDisable() {
        this.server.scheduler.cancelTask(particleTask.taskId)
        Eggs.save()
    }

    companion object {
        lateinit var plugin: JavaPlugin
        private lateinit var particleTask: BukkitTask
    }
}