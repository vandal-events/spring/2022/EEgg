package events.vandal.eegg.commands

import events.vandal.eegg.Main
import events.vandal.eegg.utils.Colors
import events.vandal.eegg.utils.Egg
import events.vandal.eegg.utils.EggTypes
import events.vandal.eegg.utils.Eggs
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.OfflinePlayer
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player
import java.io.File
import java.util.*
import kotlin.math.min

object EggCommand : CommandExecutor, TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        return when (args.size) {
            1 -> {
                val list = mutableListOf("stats", "leaderboard")
                if (sender.hasPermission("eegg.admin"))
                    list.add("admin")

                list
            }

            2 -> {
                if (args[0].lowercase(Locale.getDefault()) == "stats") {
                    Main.plugin.server.offlinePlayers.map {it.name ?: ""}.toMutableList()
                } else if (args[0].lowercase(Locale.getDefault()) == "admin" && sender.hasPermission("eegg.admin")) {
                    mutableListOf("set", "movehere", "change", "reset-acquired", "list", "remove", "del", "delete", "rotate", "id")
                } else {
                    mutableListOf("")
                }
            }

            3 -> {
                if (args[0] != "admin" || !sender.hasPermission("eegg.admin"))
                    return mutableListOf("")

                return when (args[1].lowercase(Locale.getDefault())) {
                    "set" -> {
                        EggTypes.values().map { it.name.lowercase(Locale.getDefault()) }.toMutableList()
                    }

                    "change",
                    "movehere", "remove", "del", "delete", "rotate" -> {
                        Eggs.presents.keys.toMutableList()
                    }

                    "reset-acquired" -> {
                        val list = mutableListOf("-a")
                        list.addAll(Main.plugin.server.onlinePlayers.map { it.name })
                        list.addAll(Main.plugin.server.offlinePlayers.map {it.name ?: ""})

                        list
                    }

                    else -> mutableListOf("")
                }
            }

            4 -> {
                if (args[0] != "admin" || !sender.hasPermission("eegg.admin"))
                    return mutableListOf("")

                return when (args[1].toLowerCase()) {
                    "change" -> EggTypes.values().map { it.name.toLowerCase() }.toMutableList()
                    "reset-acquired" -> {
                        val list = mutableListOf("-a")
                        list.addAll(Main.plugin.server.offlinePlayers.map {it.name ?: ""})

                        list
                    }

                    else -> mutableListOf("")
                }
            }

            else -> {
                if (args[1].toLowerCase() == "reset-acquired" && args[0].toLowerCase() == "admin" && sender.hasPermission("eegg.admin")) {
                    val list = mutableListOf("-a")
                    list.addAll(Main.plugin.server.offlinePlayers.map {it.name ?: ""})

                    list
                } else
                    mutableListOf("")
            }
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return false

        when (args[0].toLowerCase()) {
            "stats" -> {
                if (args.size == 2 && !Main.plugin.server.onlinePlayers.any { it.name == args[1] } && !Main.plugin.server.offlinePlayers.any { it.name == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That player does not exist!")
                    return true
                }

                val player = if (args.size == 2)
                    try {
                        Main.plugin.server.offlinePlayers.first { it.name?.lowercase(Locale.getDefault()) == args[1].lowercase(
                            Locale.getDefault()
                        ) }
                    } catch (_: Exception) {
                        sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That player does not exist!")
                        return true
                    }
                else {
                    if (sender !is Player) {
                        sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not a player!")
                        return true
                    }

                    sender
                }

                val presentStats = collectStats(player)

                if (presentStats.isEmpty()) {
                    sender.sendMessage("${Colors.PREFIX}Player ${ChatColor.YELLOW}${player.name}${ChatColor.RESET} doesn't seem to have collected any eggs. Please encourage them to collect some!")
                    return true
                }

                sender.sendMessage("${Colors.PREFIX}Presents collected by ${ChatColor.YELLOW}${player.name}${ChatColor.RESET}:")
                presentStats.forEach {
                    sender.sendMessage("    ${ChatColor.GREEN}${presentStats.indexOf(it) + 1}. ${colorFromType(it.type)}${it.name} ${if (player == (sender as Player)) "${ChatColor.GOLD}(${it.position.blockX} ${it.position.blockY} ${it.position.blockZ})" else ""}")
                }

                sender.sendMessage("${ChatColor.GREEN}Total collected presents by ${ChatColor.YELLOW}${player.name}${ChatColor.GREEN}: ${ChatColor.AQUA}${presentStats.size} ${ChatColor.YELLOW}/ ${ChatColor.DARK_AQUA}${Eggs.presents.size}")

                return true
            }

            "leaderboard" -> {
                val players = Main.plugin.server.offlinePlayers.toMutableList().filter {
                    val stats = collectStats(it)
                    stats.isNotEmpty()
                }

                val statsList = players.map {
                    val stats = collectStats(it)
                    Pair(it, stats.size)
                }.sortedByDescending { it.second }.subList(0, min(9.0, players.size.toDouble() - 1).toInt())

                    //println(statsList.joinToString(", ") { it.first.name })

                sender.sendMessage("${Colors.PREFIX}Collected presents leaderboard :")
                statsList.forEach {
                    sender.sendMessage("    ${ChatColor.GREEN}${statsList.indexOf(it) + 1}. ${if (it.first.isOnline) ChatColor.GREEN else ChatColor.RED}${it.first.name} ${ChatColor.DARK_GREEN} - ${ChatColor.YELLOW}${it.second}")
                }

                return true
            }

            "admin" -> {
                if (args.size == 1) return false

                when (args[1].toLowerCase()) {
                    "set" -> {
                        if (sender !is Player) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                            return true
                        }

                        try {
                            val type = if (args.size == 2) EggTypes.values()
                                .random() else EggTypes.valueOf(args[2].toUpperCase())

                            val id = randomString(6)

                            val present = Egg(
                                id,
                                name = if (args.size == 3) "PresentID$id" else args.slice(3 until args.size).joinToString(" "),
                                type = type,
                                position = sender.location,
                                world = sender.world,
                                acquired = mutableListOf()
                            )

                            Eggs.presents[id] = present
                            Eggs.spawnPresent(id)

                            Eggs.save()

                            sender.sendMessage("${Colors.PREFIX}Egg created with ID ${ChatColor.YELLOW}$id${ChatColor.RESET}!")
                        } catch (e: IllegalArgumentException) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Invalid egg type!")
                        } catch (e: Exception) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An exception occurred whilst setting the egg: ${ChatColor.DARK_RED}${e.message}")
                            e.printStackTrace()
                        }

                        return true
                    }

                    "movehere" -> {
                        if (sender !is Player) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                            return true
                        }
                        
                        if (args.size == 2)
                            return false

                        val present = Eggs.presents[args[2]]

                        if (present == null) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The specified egg doesn't exist!")
                            return true
                        }

                        present.position.block.type = Material.AIR
                        
                        present.position.set(sender.location.x, sender.location.y, sender.location.z)

                        Eggs.spawnPresent(present.id)

                        sender.sendMessage("${Colors.PREFIX}Position for egg modified!")

                        return true
                    }

                    "change" -> {
                        if (args.size == 2)
                            return false

                        try {
                            val present = Eggs.presents[args[2]]

                            if (present == null) {
                                sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The specified egg doesn't exist!")
                                return true
                            }

                            present.type =
                                if (args.size == 4) EggTypes.valueOf(args[3].toUpperCase()) else EggTypes.values()
                                    .random()

                            sender.sendMessage("${Colors.PREFIX}Changed the egg type to ${colorFromType(present.type)}${present.type.name}${ChatColor.RESET}!")

                            Eggs.spawnPresent(present.id)

                            return true
                        } catch (e: IllegalArgumentException) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Invalid egg type!")

                            return true
                        } catch (e: Exception) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An exception occurred whilst changing the egg type: ${ChatColor.DARK_RED}${e.message}")
                            return true
                        }
                    }

                    "reset-acquired" -> {
                        if (!sender.hasPermission("eegg.admin.reset")) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to reset acquired status!")
                            return true
                        }

                        if (args.size == 2) {
                            return false
                        }

                        if (args[2].toLowerCase() == "-a") {
                            Eggs.presents.forEach {
                                it.value.acquired.clear()
                            }

                            Main.plugin.server.offlinePlayers.forEach { offlinePlayer ->
                                Bukkit.getServer().advancementIterator().forEachRemaining { advancement ->
                                    if (advancement.key.key.startsWith("eegg")) {
                                        if (offlinePlayer.isOnline) {
                                            val adv = offlinePlayer.player?.getAdvancementProgress(advancement)
                                            adv?.awardedCriteria?.forEach { criteria ->
                                                adv.revokeCriteria(criteria)
                                            }
                                        } else {
                                            try {
                                                File(
                                                    Main.plugin.server.worldContainer,
                                                    "${Main.plugin.server.worlds[0].name}/advancements/${offlinePlayer.uniqueId}.json"
                                                ).delete()
                                            } catch (e: Exception) {
                                                println("Unable to delete advancement for ${offlinePlayer.name}/${offlinePlayer.uniqueId}")
                                                e.printStackTrace()
                                            }
                                        }
                                    }
                                }
                            }

                            Eggs.save()
                            sender.sendMessage("${Colors.PREFIX}Reset all acquired status.")

                            return true
                        }

                        args.toList().subList(2, args.size).forEach {
                            val player = Main.plugin.server.getPlayer(it) ?: Main.plugin.server.getOfflinePlayerIfCached(it)?.player

                            if (player == null) {
                                sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Egg ${ChatColor.YELLOW}$it ${ChatColor.RED}doesn't exist!")
                                return true
                            }

                            Eggs.presents.forEach { itt ->
                                itt.value.acquired.remove(player.uniqueId)
                            }

                            Bukkit.getServer().advancementIterator().forEachRemaining { advancement ->
                                if (advancement.key.key.startsWith("eegg")) {
                                    if (player.isOnline) {
                                        val adv = player.player?.getAdvancementProgress(advancement)
                                        adv?.awardedCriteria?.forEach { criteria ->
                                            adv.revokeCriteria(criteria)
                                        }
                                    } else {
                                        try {
                                            File(
                                                Main.plugin.server.worldContainer,
                                                "${Main.plugin.server.worlds[0].name}/advancements/${player.uniqueId}.json"
                                            ).delete()
                                        } catch (e: Exception) {
                                            println("Unable to delete advancement for ${player.name}/${player.uniqueId}")
                                            e.printStackTrace()
                                        }
                                    }
                                }
                            }

                            Eggs.save()

                            sender.sendMessage("${Colors.PREFIX}Reset acquired status for ${ChatColor.YELLOW}$it${ChatColor.RESET}!")
                        }

                        return true
                    }

                    "list" -> {
                        sender.sendMessage("${Colors.PREFIX}Listing all eggs...")

                        Eggs.presents.forEach {
                            sender.sendMessage("    - ${colorFromType(it.value.type)}${it.value.name} ${ChatColor.YELLOW}(ID : ${it.key}) ${ChatColor.GOLD}(${it.value.position.blockX} ${it.value.position.blockY} ${it.value.position.blockZ})")
                        }

                        return true
                    }

                    "remove", "del", "delete" -> {
                        if (args.size == 2) return false

                        val id = args[2]

                        val present = Eggs.presents[id]

                        if (present == null) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The egg doesn't exist!")
                            return true
                        }

                        sender.sendMessage("${Colors.PREFIX}Successfully removed ${colorFromType(present.type)}${present.name} ${ChatColor.YELLOW}(ID : $id) ${ChatColor.GOLD}(${present.position.blockX} ${present.position.blockY} ${present.position.blockZ})")

                        present.position.block.type = Material.AIR
                        Eggs.presents.remove(id, present)
                        Eggs.save()

                        return true
                    }

                    "rotate" ->  {
                        sender.sendMessage("${ChatColor.GREEN}Tip${ChatColor.GOLD}: ${ChatColor.AQUA}Use the ${ChatColor.BLUE}Debug Stick ${ChatColor.AQUA}to rotate the egg. It's far faster.")

                        return true
                    }

                    "id" -> {
                        if (sender !is Player) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                            return true
                        }

                        val targetedBlock = sender.getTargetBlock(10)

                        if (targetedBlock == null || !Eggs.presents.any { it.value.position.block == targetedBlock }) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not facing a egg! To use this command, you must be looking at a egg that is within 10 blocks away.")
                            return true
                        }

                        val present = Eggs.presents.filter { it.value.position.block == targetedBlock }.values.first()

                        sender.sendMessage("${Colors.PREFIX}The egg you're looking at is ${colorFromType(present.type)}${present.name} ${ChatColor.GOLD}(ID : ${present.id})${ChatColor.RESET}.")

                        return true
                    }

                    else -> return false
                }
            }
        }

        return false
    }

    fun collectStats(player: OfflinePlayer): List<Egg> {
        val list = Eggs.presents.filter { it.value.acquired.contains(player.uniqueId) }

        return list.values.toList()
    }

    fun colorFromType(type: EggTypes): ChatColor {
        return when (type) {
            EggTypes.GREEN,
            EggTypes.GREEN2 -> ChatColor.GREEN

            EggTypes.GOLD -> ChatColor.GOLD

            EggTypes.CYAN -> ChatColor.DARK_AQUA

            EggTypes.BLUE -> ChatColor.BLUE
            EggTypes.RED -> ChatColor.RED
        }
    }

    private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    private fun randomString(length: Int): String {
        return (1..length).map {
            kotlin.random.Random.nextInt(0, charPool.size)
        }
            .map(charPool::get)
            .joinToString("")
    }
}

/*
Admin commands:

set [type] [name]
movehere <id>
change <id> [type]
reset-acquired <name / -a>
 */