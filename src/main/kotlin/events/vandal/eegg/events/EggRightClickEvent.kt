package events.vandal.eegg.events

import com.destroystokyo.paper.event.block.BlockDestroyEvent
import events.vandal.eegg.Main
import events.vandal.eegg.commands.EggCommand
import events.vandal.eegg.utils.Colors
import events.vandal.eegg.utils.Eggs
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.TextColor
import org.bukkit.ChatColor
import org.bukkit.GameRule
import org.bukkit.NamespacedKey
import org.bukkit.Sound
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.EquipmentSlot

object EggRightClickEvent : Listener {
    @EventHandler
    fun onPresentRightClick(ev: PlayerInteractEvent) {
        if (ev.action == Action.RIGHT_CLICK_BLOCK && Eggs.presents.any { it.value.position.block == ev.clickedBlock } && ev.hand == EquipmentSlot.HAND && !ev.hasItem()) {
            ev.isCancelled = true
            val present = Eggs.presents.filter { it.value.position.block == ev.clickedBlock }.values.first()

            if (present.acquired.contains(ev.player.uniqueId)) {
                ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}You have already acquired this easter egg!")
                return
            }

            ev.player.playSound(ev.player.location, Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F)
            ev.player.sendMessage("${Colors.PREFIX}You have acquired the \"${EggCommand.colorFromType(present.type)}${present.name}${ChatColor.RESET}\" present!")

            present.acquired.add(ev.player.uniqueId)

            if (EggCommand.collectStats(ev.player).size == 1) {
                ev.player.getAdvancementProgress(Main.plugin.server.getAdvancement(NamespacedKey.fromString("eepresent/wake_up_call", Main.plugin)!!)!!).awardCriteria("0")
            }

            if (EggCommand.collectStats(ev.player).size == Eggs.presents.size) {
                ev.player.getAdvancementProgress(Main.plugin.server.getAdvancement(NamespacedKey.fromString("eepresent/all_eggs", Main.plugin)!!)!!).awardCriteria("0")

                if (!ev.player.world.getGameRuleValue(GameRule.ANNOUNCE_ADVANCEMENTS)!!) {
                    Main.plugin.server.broadcast(Component.translatable("chat.type.advancement.challenge", ev.player.displayName(), Component.text("[How the Grinch Stole Christmas]").color(TextColor.color(170, 0, 170)).hoverEvent(Component.text("Coming Out the Closet\nCollect every easter egg in the map.").color(TextColor.color(170, 0, 170)))))
                }
            }

            Eggs.presents[present.id] = present

            Eggs.save()
        }
    }

    @EventHandler
    fun onPresentBreak(ev: BlockBreakEvent) {
        if (Eggs.presents.any { it.value.position.block == ev.block }) {
            ev.isCancelled = true

            ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}Don't break the present!")
        }
    }

    @EventHandler
    fun onPresentDestroy(ev: BlockDestroyEvent) {
        if (Eggs.presents.any { it.value.position.block == ev.block }) {
            ev.isCancelled = true
        }
    }
}