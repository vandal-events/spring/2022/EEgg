package events.vandal.eegg.events

// This code kinda doesn't work anymore, because it was using the old armor stand system.
// Now it uses the block system, which I should've thought of beforehand.
// I kept the code though for future reference.
/*
import com.gitlab.mcyttwthp.eepresent.commands.PresentCommand
import com.gitlab.mcyttwthp.eepresent.utils.Colors
import com.gitlab.mcyttwthp.eepresent.utils.Presents
import org.bukkit.ChatColor
import org.bukkit.Sound
import org.bukkit.entity.ArmorStand
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractAtEntityEvent
import org.bukkit.event.player.PlayerInteractEntityEvent

object ArmorStandRightClickEvent : Listener {
    @EventHandler
    fun onRightClickEvent(ev: PlayerInteractAtEntityEvent) {
        if (ev.rightClicked is ArmorStand && Presents.presents.any { it.value.entity == ev.rightClicked }) {
            ev.isCancelled = true
            val present = Presents.presents.filter { it.value.entity == ev.rightClicked }.values.first()

            if (present.acquired.contains(ev.player)) {
                ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}You have already acquired this present!")
                return
            }

            ev.player.playSound(ev.player.location, Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F)
            ev.player.sendMessage("${Colors.PREFIX}You have acquired the \"${PresentCommand.colorFromType(present.type)}${present.name}${ChatColor.RESET}\" present!")

            present.acquired.add(ev.player)

            Presents.presents[present.id] = present

            Presents.save()
        }
    }

    @EventHandler
    fun onRightClick2Event(ev: PlayerInteractEntityEvent) { // I have no fucking idea why, but there are two events for this
        if (ev.rightClicked is ArmorStand && Presents.presents.any { it.value.entity == ev.rightClicked }) {
            ev.isCancelled = true
            val present = Presents.presents.filter { it.value.entity == ev.rightClicked }.values.first()

            if (present.acquired.contains(ev.player)) {
                ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}You have already acquired this present!")
                return
            }

            ev.player.playSound(ev.player.location, Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F)
            ev.player.sendMessage("${Colors.PREFIX}You have acquired the \"${PresentCommand.colorFromType(present.type)}${present.name}${ChatColor.RESET}\" present!")

            present.acquired.add(ev.player)

            Presents.presents[present.id] = present

            Presents.save()
        }
    }
}*/